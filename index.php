
<?php
require('animal.php');
$sheep = new Animal("shaun");

echo "Name : " . $sheep->name; // "shaun"
echo "<br>Legs : " . $sheep->legs;
echo "<br>Cold Blooded : " . $sheep->cold_blooded;
echo "<br><br>";
// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

require('frog.php');
$kodok = new Frog("buduk");
echo "Name : " . $kodok->name;
echo "<br>Legs : " . $kodok->legs;
echo "<br>Cold Blooded : " . $kodok->cold_blooded . "<br>";
$kodok->jump(); // "hop hop"
echo "<br><br>  ";

require('ape.php');
$sungokong = new Ape("kera sakti");
echo "Name : " . $sungokong->name;
echo "<br>Legs : " . $sungokong->legs;
echo "<br>Cold Blooded : " . $sungokong->cold_blooded . "<br>";
$sungokong->yell(); // "Auooo"


?>

